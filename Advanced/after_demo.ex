defmodule AfterDemo do
  def func(filename) do
    case File.open(filename) do
      {:ok, file} ->
        try do
          IO.puts("Opened #{filename}")
        after
          File.close(file)
        end

      {:error, reason} ->
        IO.puts("error: #{inspect(reason)}")
    end
  end
end