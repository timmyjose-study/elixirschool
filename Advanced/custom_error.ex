defmodule CustomError do
  defexception message: "a custom error has occurred"
end

defmodule CustomErrorDemo do
  def run do
    try do
      raise CustomError
    rescue
      e in CustomError -> IO.puts("#{inspect(e)}")
    end

    try do
      raise CustomError, message: "Something awful has just happened. Hjalp!"
    rescue
      e in CustomError -> IO.puts("#{inspect(e)}")
    end
  end
end