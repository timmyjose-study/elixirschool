defmodule CheckOdd do
  defmacro odd(number, do: do_clause, else: else_clause) do
    quote do
      if rem(unquote(number), 2) == 1, do: unquote(do_clause), else: unquote(else_clause)
    end
  end
end