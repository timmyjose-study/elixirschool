defmodule Greeter4 do
  def hello(name), do: phrase() <> name
  defp phrase(), do: "Hello, "
end