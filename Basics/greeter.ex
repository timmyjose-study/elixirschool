defmodule Greeter do
  def greet(name) do
    IO.puts("Hello, #{name}")
  end

  def say_hello(name), do: "Hello, " <> name
end