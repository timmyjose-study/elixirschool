defmodule SendingProcess do
  @moduledoc """
    A simple module that sends a ping to a process.
  """

  def run(pid) do
    send(pid, :ping)
  end
end
