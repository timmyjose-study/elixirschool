defmodule Calculator do
  @moduledoc """
  A simple mathematical calculator.

  ## Examples

      iex> Calculator.Add.add(1, 2)
      3

      iex> Calculator.Sub.sub(1, 2)
      -1

      iex> Calculator.Mult.mult(11, 2)
      22

      iex> Calculator.Div.divide(11, 2)
      5

  """
end

defmodule Calculator.Add do
  def add(x, y) when is_number(x) and is_number(y) do
    x + y
  end
end

defmodule Calculator.Sub do
  def sub(x, y) when is_number(x) and is_number(y) do
    x - y
  end
end

defmodule Calculator.Mult do
  def mult(x, y) when is_number(x) and is_number(y) do
    x * y
  end
end

defmodule Calculator.Div do
  def divide(x, y) when is_number(x) and is_number(y) do
    case y do
      0 -> 0
      _ -> div(x, y)
    end
  end
end
