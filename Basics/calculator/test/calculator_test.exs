defmodule CalculatorTest do
  use ExUnit.Case
  doctest Calculator

  test "adds numbers correctly" do
    assert Calculator.Add.add(1, 2) == 3
  end

  test "subtracts numbers correctly" do
    assert Calculator.Sub.sub(1, 2) == -1
  end

  test "multiplies numbers correctly" do
    assert Calculator.Mult.mult(11, 2) == 22
  end

  test "divides numbers correctly" do
    assert Calculator.Div.divide(11, 2) == 5
  end

  test "divides by zero correctly" do
    assert Calculator.Div.divide(11, 0) == 0
  end
end
