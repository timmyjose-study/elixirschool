defmodule SendingProcessTest do
  use ExUnit.Case
  doctest SendingProcess

  test "sends a ping to a valid process" do
    SendingProcess.run(self())
    assert_received :ping
  end
end
