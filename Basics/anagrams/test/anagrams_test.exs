defmodule AnagramsTest do
  use ExUnit.Case
  doctest Anagrams

  test "positive tests" do
    assert Anagrams.anagrams?("hello", "olleh")
    assert Anagrams.anagrams?("super", "perus")
    assert Anagrams.anagrams?("hello", "Hello")
  end

  test "negative tests" do
    refute Anagrams.anagrams?("super", "pesus")
    refute Anagrams.anagrams?("hello", "ollh")
  end
end
