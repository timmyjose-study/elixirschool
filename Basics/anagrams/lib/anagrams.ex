defmodule Anagrams do
  @moduledoc """
  A simple anagrams checker.
  """

  def anagrams?(a, b) when is_binary(a) and is_binary(b) do
    sort_string(a) == sort_string(b)
  end

  defp sort_string(s) when is_binary(s) do
    s
    |> String.downcase
    |> String.graphemes
    |> Enum.sort
  end
end

