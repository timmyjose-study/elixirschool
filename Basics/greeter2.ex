defmodule Greeter2 do
  def hello(), do: "Hello, anonymous fan"
  def hello(name), do: "Hello, #{name}"
  def hello(name1, name2), do: "Hello, #{name1} and #{name2}"
end