defmodule Example.Greetings do
  def morning(name) do
    "Good morning, #{name}"
  end

  def evening(name) do
    "Good evening, #{name}"
  end
end