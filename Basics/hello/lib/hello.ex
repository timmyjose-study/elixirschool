defmodule Hello do
  @moduledoc """
  Documentation for `Hello`.
  """

  @doc """
  Say "Hello, world" every time.
  """
  def say do
    IO.puts("Hello, world!")
  end
end
