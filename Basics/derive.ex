defmodule DeriveDemo do
  @derive {Inspect, only: [:name]}
  defstruct name: nil, roles: []
end