defmodule Example do
  @moduledoc """
  Documentation for `Example`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Example.hello()
      :world

  """
  def hello do
    :world
  end
end

defmodule CustomSigils do
  def sigil_p(string, []), do: String.upcase(string)
end
