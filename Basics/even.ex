import Integer

defmodule EvenDemo do
  def demo do
    m = %{a: 1, c: 3}

    with {:ok, number} <- Map.fetch(m, :a),
         true <- is_even(number) do
      IO.puts("#{number} is even")
      :even
    else
      :error -> 
        IO.puts("number not found in map")
        :error

      _ ->
        IO.puts("found an odd number")
    end
  end
end