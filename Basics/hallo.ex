defmodule Hallo do
  defmacro __using__(opts) do
    greeting = Keyword.get(opts, :greeting, "Hello")

    quote do
      def hello(name), do: unquote(greeting) <> ", " <> name
    end
  end
end

defmodule Example do
  use Hallo
end

defmodule AnotherExample do
  use Hallo, greeting: "Privet"
end