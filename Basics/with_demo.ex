# `with` is basically `let`

defmodule WithDemo do
  def with_demo do
    bob = %{name: "Bob", age: 42}

    with {:ok, name} <- Map.fetch(bob, :name),
         {:ok, age} <- Map.fetch(bob, :age) do
      IO.puts("This is #{name}, aged #{age}")
    end
  end
end