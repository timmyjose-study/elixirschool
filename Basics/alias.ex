defmodule Sayings.Greetings do
  def basic(name), do: "Hi, #{name}"
end

defmodule Sayings.Farewells do
  def basic(name), do: "Bye, #{name}"
end

defmodule Example do
  alias Sayings.{Greetings, Farewells}

  def greeting(name), do: Greetings.basic(name)
  def farewell(name), do: Farewells.basic(name)
end