defmodule Attrs do
  @greeting "Hello"

  def greet(name) do
    @greeting <> ", " <> name
  end
end